# Hello World
The sierra hello world program is very simple at just two lines (minus comments).

```sierra
# import the io functions from the standard library
import std.console.*

# output text to stdout
printLine("Hello, World!")
```

If you don't want to import the whole namespace you can specifically import `printLine`

```sierra
import std.console.printLine

printLine("Hello, World!")
```
